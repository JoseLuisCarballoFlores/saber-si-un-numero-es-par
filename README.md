
#include <stdio.h>  
#include <stdlib.h> 
#include <math.h>

int main(){

int x, y;

printf("Escribe un número ");
scanf("%i", &x);

y = x % 2; //Guardo en la variable y el resultado de la operacion de x %(resto) 2

if(y==0){  //Aquí utilizo la estructura selectiva if, y digo que si se cumple que y es = a 0, el número es par.
    printf("El número es par %i", x); //Si y es = a 0 se ejecutará esta línea.
}

else{ //si no se cumple el if se cumple el else y se ejecutará lo que hay dentro del else
    printf("El número es impar %i", x);
}


return 0;
}
